namespace Task19Noroff.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class seedDataSupervisor : DbMigration
    {
        public override void Up()
        {
            Sql("INSERT INTO Supervisors(Name) VALUES('Per Hansen')");
            Sql("INSERT INTO Supervisors(Name) VALUES('David Davidsen')");
            Sql("INSERT INTO Supervisors(Name) VALUES('Jan Jansen')");
        }
        
        public override void Down()
        {
            Sql("DELETE FROM Supervisors WHERE Name ='Per Hansen'");
            Sql("DELETE FROM Supervisors WHERE Name ='David Davidsen'");
            Sql("DELETE FROM Supervisors WHERE Name ='Jan Jansen'");
        }
    }
}
